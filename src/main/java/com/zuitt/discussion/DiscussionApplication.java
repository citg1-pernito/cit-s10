package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

//	Retrieve all posts
//	localhost:8080/posts
//	@RequestMapping(value = "/posts", method = RequestMethod.GET)
	@GetMapping("/posts")
	public String getPosts(){
		return "All posts retrieved.";
	}

//	Creating a new post
//	localhost:8080/posts
//	@RequestMapping(value = "/posts", method = RequestMethod.POST)
	@PostMapping("/posts")
	public String createPost(){
		return "New post created.";
	}

//	Retrieving a single post
//	localhost:8080/posts/1234
//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.GET)
	@GetMapping("/posts/{postid}")
	public String getPost(@PathVariable Long postid){
		return "Viewing details of post " + postid;
	}

//	Deleting a post
//	localhost:8080/posts/1234
//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
	@DeleteMapping("/posts/{postid}")
	public String deletePost(@PathVariable Long postid){
		return "The post " + postid + " has been deleted.";
	}

//	Updating a post
//	localhost:8080/posts/1234
//	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
	@PutMapping("/posts/{postid}")
//	Automatically converts the format to JSON
	@ResponseBody
	public Post updatePost(@PathVariable Long postid, @RequestBody Post post){
		return post;
	}

//	Retrieving a post for a particular user
//	localhost:8080/myPosts
//	@RequestMapping(value = "/myPosts", method = RequestMethod.GET)
	@GetMapping("/myPosts")
	public String getMyPosts(@RequestHeader(value = "Authorization") String user){
		return "Posts for " + user + " have been retrieved";
	}


	@GetMapping("/users")
	public String getUsers(){
		return "All users retrieved.";
	}

	@PostMapping("/users")
	public String createUser(){
		return "New user created.";
	}

	@GetMapping("/users/{userid}")
	public String getUser(@PathVariable Long userid){
		return "Viewing details of user " + userid;
	}

//	@DeleteMapping("/users/{userid}")
//	public String deleteUser(@PathVariable Long userid){
//		if(request.headers().get("Authorization").isEmpty()){
//			return "Unauthorized access";
//		}
//		else {
//			return "The user " + userid + " has been deleted.";
//		}
//	}

	@DeleteMapping("/users/{userid}")
	public ResponseEntity<String> deleteUser(@PathVariable Long userid, @RequestHeader(value="Authorization", required=false) String authorizationHeader){
		if(authorizationHeader != null && !authorizationHeader.isEmpty()){
			// Your code to delete the user goes here
			return new ResponseEntity<>("The user " + userid + " has been deleted.", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Unauthorized access.", HttpStatus.UNAUTHORIZED);
		}
	}

	@PutMapping("/users/{userid}")
	@ResponseBody
	public User updateUser(@PathVariable Long userid, @RequestBody User user){
		return user;
	}

}
